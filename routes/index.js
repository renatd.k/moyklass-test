const moment = require('moment');
const express = require('express');
const router = express.Router();

const db = require("../models");

const Teacher = db.teacher;
const Student = db.student;
const Lesson = db.lesson;
const Op = db.Op;
const sequelize = db.sequelize;

router.get('/', function(req, res, next) {

  let where = {};
  
  if (req.query.status) {
    where.status = req.query.status;
  }

  const date = req.query.date ? req.query.date.split(",") : [];
  if (date.length == 1) {
    where.date = { [Op.between]: [moment(date[0]), moment(date[0]).add(1,"d")] };
  }
  if (date.length == 2) {
    where.date = { [Op.between]: [moment(date[0]), moment(date[1]).add(1,"d")] };
  }
  
  let teacher = { model: Teacher, as: "teachers"};
  if (req.query.teacherIds) {
    teacher.where = { id: {[Op.in]: req.query.teacherIds.split(",")} };
  }  
  
  const studentsCount = req.query.studentsCount ? req.query.studentsCount.split(",") : [];
  if (studentsCount.length == 1) {
    where.students_count = { [Op.eq]: studentsCount[0] };
  }
  if (studentsCount.length > 1) {
    where.students_count = { [Op.between]: studentsCount };
  }
  
  let offset = 0;
  let limit = 5;
  if (req.query.lessonsPerPage) {
    limit = req.query.lessonsPerPage;
  } 

  if (req.query.page) {
    offset  = ( req.query.page - 1 ) * limit; 
  } 

  Lesson.findAll({
    include: [
        teacher,
        {
            model: Student,
            as: "students",
        }
    ],
    where,
    offset,
    limit,
  }).then((lessons) => {
    res.json(lessons);
  }); 

});

router.post('/lessons', async function(req, res, next) {
  
  const where = { id: { [Op.in]: req.body.teacherIds } };
  const teachers = await Teacher.findAll({where});
  
  let currentDate = moment(req.body.firstDate);
  let title = req.body.title;
  let days = req.body.days;
  let lessonsCount = req.doby.lessonsCount;
  let lastDate = req.body.lastDate;
  let createdLessonsCount = 0;
  let lessonIds = [];

  for (let i=0; i < 365; i++) {
    if (days.includes(currentDate.day())) {
      const lesson = await Lesson.create({
        title,
        date: currentDate.format("YYYY-MM-DD"),
      });
      await lesson.addTeachers(teachers);
      lessonIds.push(lesson.id);
      createdLessonsCount++;
    }
    if (lessonsCount && createdLessonsCount >= lessonsCount) {
      break; 
    } 
    if (createdLessonsCount >= 300) {
      break;
    }
    if ( currentDate.isSame(lastDate) || currentDate.isAfter(lastDate) ) {
      break;
    }
    currentDate.add(1,"d");
  }  
 
  res.json(lessonIds);
});



module.exports = router;
