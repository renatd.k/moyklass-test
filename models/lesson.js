'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class lesson extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      lesson.belongsToMany(models.student, { through: "lesson_students", as: "students", foreignKey: "lesson_id" });
      lesson.belongsToMany(models.teacher, { through: "lesson_teachers", as: "teachers", foreignKey: "lesson_id" });
    }
  };
  lesson.init({
    date: DataTypes.DATE,
    title: DataTypes.STRING,
    status: DataTypes.INTEGER,
    students_count: DataTypes.INTEGER,
    visit_count: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'lesson',
  });
  return lesson;
};
