CREATE OR REPLACE FUNCTION process_lesson_students() RETURNS TRIGGER AS $lesson_students$

BEGIN
		UPDATE lessons l
		SET 
			students_count = (SELECT  COUNT( DISTINCT student_id ) FROM lesson_students WHERE lesson_id = NEW.lesson_id),
			visit_count = (SELECT COUNT( DISTINCT student_id ) FROM lesson_students WHERE lesson_id = NEW.lesson_id AND visit)
		WHERE 
		 	l.id = NEW.lesson_id;
		RETURN NEW;
END;

$lesson_students$ LANGUAGE plpgsql;

