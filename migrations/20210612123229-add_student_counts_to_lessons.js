'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'lessons',
      'students_count',
     Sequelize.INTEGER
    );
    await queryInterface.addColumn(
      'lessons',
      'visit_count',
     Sequelize.INTEGER
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      'lessons',
      'students_count'
    );
    await queryInterface.removeColumn(
      'lessons',
      'visit_count'
    );
  }
};
